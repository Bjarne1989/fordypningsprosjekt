#include <Servo.h>
#include <Wire.h>

//Outputs to the ESC
#define FR_PROP_PIN 10
#define FL_PROP_PIN 3
#define RR_PROP_PIN 9
#define RL_PROP_PIN 11

//Gyreoscope
#define GYR_ADDRESS (0xD2 >> 1)
#define L3G4200D_OUT_X_L 0x28
#define L3G4200D_CTRL_REG1 0x20
#define L3G4200D_CTRL_REG4 0x23

const double toDps = 0.0175;

//Accelerometer
#define ACC_DATA_REG 0x32
#define ACC_ADDRESS 0x53
#define ACC_PWR_CTL 0x2D

const float xAccOff = -0.07;
const float yAccOff = 0.03;
const float zAccOff = -0.06;

//Compass
#define COM_ADDRESS 0x1E
#define COM_CONFIG_A 0x00
#define COM_CONFIG_B 0x01
#define COM_MODE_REG 0x02
#define COM_DATA_REG 0x03

//http://diydrones.com/profiles/blogs/advanced-hard-and-soft-iron-magnetometer-calibration-for-dummies
//https://www.youtube.com/watch?v=Fsd6dAuRURo
const float xComOff = (132.737 - 170.0)/0.92;     // Offset required to adjust x coordinate to zero origin
const float yComOff = (-6.948 + 82.0 + 38.0)/0.92;        // Offset required to adjust y coordinate to zero origin
const float zComOff = (202.026 + 271-500)/0.92;        // Offset required to adjust y coordinate to zero origin
const float declination = 0.04886;    // Enter magnetic declination [rad] +2* 48' -> 2.8*PI/180

Servo frProp;
Servo flProp;
Servo rrProp;
Servo rlProp;

long timer = 0;
void setup(){
  Serial.begin(38400);
  Wire.begin();
  Serial.println("Init");
  
  //Acc
  initAccelorometer();
  delay(500);
  
  //Gyr
  initGyro();
  delay(500);
  
  //Compass
  initCompass();

  frProp.attach(FR_PROP_PIN, 1000, 2000);
  frProp.writeMicroseconds(1000);
  flProp.attach(FL_PROP_PIN, 1000, 2000);
  flProp.writeMicroseconds(1000);
  rrProp.attach(RR_PROP_PIN, 1000, 2000);
  rrProp.writeMicroseconds(1000);
  rlProp.attach(RL_PROP_PIN, 1000, 2000);
  rlProp.writeMicroseconds(1000);
  
  Serial.println("DONE");
}

void loop(){
  timer = millis();
  
  //Gyro
  double xGyr, yGyr, zGyr;
  getGyrXYZ(&xGyr, &yGyr, &zGyr);

  //Acc
  double xAcc, yAcc, zAcc;
  getAccXYZ(&xAcc, &yAcc, &zAcc);
  
  xAcc += xAccOff;
  yAcc += yAccOff;
  zAcc += zAccOff;
  
  double phi = atan2(yAcc,zAcc);
  double theta = -atan2(xAcc,zAcc);
  
//  Serial.print(" phi:");Serial.print(phi);
//  Serial.print(" theta:");Serial.print(theta);
  
  //Compass
  int xCom, yCom, zCom;
  getComXYZ(&xCom, &yCom, &zCom);
  float gScale = .92;  // Scale factor for +1.3Ga setting
  xCom += xComOff;
  yCom += yComOff;
  zCom += zComOff;
  int xc = xCom * gScale;
  int yc = yCom * gScale;
  int zc = zCom * gScale;
  
  double x_psi = xc*cos(theta) + zc*sin(theta);
  double y_psi = xc*sin(phi)*sin(theta) + yc*cos(phi) - zc*sin(phi)*cos(theta);
  double psi = atan2(x_psi, y_psi);
  
  if (psi < 0) psi += 2*PI;
  if (psi > 2*PI) psi -= 2*PI;
  
  //Serial.print(" psi:");Serial.print(psi);
  
  // Print raw data
//  Serial.print(" accX:");Serial.print(xAcc);
//  Serial.print(" accY:");Serial.print(yAcc);
//  Serial.print(" accZ:");Serial.print(zAcc);
//  Serial.print(" RawX = ");Serial.print(xc);
//  Serial.print(" RawY = ");Serial.print(yc);
//  Serial.print(" RawZ = ");Serial.print(zc);
//  Serial.print(xc);
//  Serial.print("\t");Serial.print(yc);
//  Serial.print("\t");Serial.print(zc);
  
  if (Serial.available()){
    String value = "";
    delay(20);
    while(Serial.available()){
      char letter = Serial.read();
      value += letter;
    }
    int power = value.toInt();
    frProp.writeMicroseconds(power + 1000);
    flProp.writeMicroseconds(power + 1000);
    rrProp.writeMicroseconds(power + 1000);
    rlProp.writeMicroseconds(power + 1000);
  }
  
    // Print data for logging
  Serial.print(xGyr);
  Serial.print("\t");Serial.print(yGyr);
  Serial.print("\t");Serial.print(zGyr);
  Serial.print("\t");Serial.print(phi);
  Serial.print("\t");Serial.print(theta);
  Serial.print("\t");Serial.print(psi);
  Serial.print("\t");Serial.print(millis() - timer);
  Serial.println();
  
  delay(15);
}


void getBytes(int adress, uint8_t *buffer, int bytes){
  Wire.requestFrom(adress, 22);
  
  uint8_t counter = 0;  
  while ((Wire.available()) && (counter < bytes)){
    buffer[counter++] = Wire.read();
  }
}

/*
*      GYRO
*/

void initGyro(){
  Wire.beginTransmission(GYR_ADDRESS);
  Wire.write(L3G4200D_CTRL_REG1);
  Wire.write(0x0F); // Normal power mode, all axes enabled
  Wire.endTransmission();
  
  Wire.beginTransmission(GYR_ADDRESS);
  Wire.write(L3G4200D_CTRL_REG4);
  Wire.write(0b00010000); // 500dps
  Wire.endTransmission();
  
}

void getGyrXYZ(double *xGyr, double *yGyr, double *zGyr){
  uint8_t rawGyr[6];
  
  Wire.beginTransmission(GYR_ADDRESS);
  Wire.write(L3G4200D_OUT_X_L | (1 << 7)); 
  Wire.endTransmission();
  Wire.requestFrom(GYR_ADDRESS, 6);
  
  getBytes(GYR_ADDRESS, (uint8_t*)rawGyr, 6);
  
  *xGyr = (double)(rawGyr[0] << 8 | rawGyr[1]) * toDps;
  *yGyr = (double)(rawGyr[2] << 8 | rawGyr[3]) * toDps;
  *zGyr = (double)(rawGyr[4] << 8 | rawGyr[5]) * toDps;
}

/*
*      COMPASS
*/

void initCompass(){
  Wire.beginTransmission(COM_ADDRESS);
  Wire.write(COM_CONFIG_A);
  Wire.write(0b01111000); // 8 samples per output, 75Hz data output rate
  Wire.endTransmission();
  
  Wire.beginTransmission(COM_ADDRESS);
  Wire.write(COM_CONFIG_B);
  Wire.write(0b00100000); // GAIN = +/- 1.3Ga, adjust if noisy signal
  Wire.endTransmission();
  
  Wire.beginTransmission(COM_ADDRESS);
  Wire.write(COM_MODE_REG);
  Wire.write(0b00000000); // NOT high speed I2C, continuous mesurement mode
  Wire.endTransmission();
}

void getComXYZ(int *xCom, int *yCom, int *zCom){
  uint8_t rawCom[6];
  
  Wire.beginTransmission(COM_ADDRESS);
  Wire.write(COM_DATA_REG);
  Wire.endTransmission();
  
  getBytes(COM_ADDRESS, (uint8_t*)rawCom, 6);
  
  *xCom = ((rawCom[0] << 8) | rawCom[1]);
  *zCom = ((rawCom[2] << 8) | rawCom[3]);
  *yCom = ((rawCom[4] << 8) | rawCom[5]);
}

/*
*      ACCELEROMETER
*/

void initAccelorometer(){
  Wire.beginTransmission(ACC_ADDRESS);
  Wire.write(ACC_PWR_CTL); //Turn on
  Wire.write(0x08);
  Wire.endTransmission();
}

void getAccXYZ(double *xAcc, double *yAcc, double *zAcc){
  byte rawAcc[6];
  
  Wire.beginTransmission(ACC_ADDRESS);
  Wire.write(ACC_DATA_REG);
  Wire.endTransmission();
  
  getBytes(ACC_ADDRESS, (uint8_t*)rawAcc, 6);
  
  *xAcc = ((rawAcc[0] | (rawAcc[1] << 8)) + 20) / 256.0;
  *yAcc = ((rawAcc[2] | (rawAcc[3] << 8)) - 15) / 256.0;
  *zAcc = ((rawAcc[4] | (rawAcc[5] << 8)) + 23) / 256.0;
}
