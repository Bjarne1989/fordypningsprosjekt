#include "sensors.h"

I2C_HandleTypeDef *i2c;

/*---------------------------- Gyreoscope Defines --------------------*/
#define GYR_ADDRESS_W 0xD2
#define GYR_ADDRESS_R (0xD2 | 1)
#define L3G4200D_OUT_X_L 0x28
#define L3G4200D_CTRL_REG1 0x20
#define L3G4200D_CTRL_REG4 0x23
const double toDps = 0.0175;

/*---------------------------- Accelerometer Defines -----------------*/
#define ACC_ADDRESS_W 0xA6
#define ACC_ADDRESS_R (0xA6 | 1)
#define ACC_DATA_REG 0x32
#define ACC_PWR_CTL 0x2D
const float xAccOff = -0.07;
const float yAccOff = 0.03;
const float zAccOff = -0.06;

/*---------------------------- Compass Defines -----------------------*/
#define COM_ADDRESS_W 0x3C
#define COM_ADDRESS_R (0x3C | 1)
#define COM_CONFIG_A 0x00
#define COM_CONFIG_B 0x01
#define COM_MODE_REG 0x02
#define COM_DATA_REG 0x03
const float gScale = .92;              // Scale factor for +1.3Ga setting
const float xComOff = -37.26/0.92;     // Offset required to adjust x coordinate to zero origin
const float yComOff = 113.05/0.92;     // Offset required to adjust y coordinate to zero origin
const float zComOff = -26.97/0.92;     // Offset required to adjust z coordinate to zero origin

/*---------------------------- Barometer Defines ---------------------*/
#define BAR_ADDRESS_W 0xEE
#define BAR_ADDRESS_R (0xEE | 1)
#define BAR_DATA_REG 0xF6
#define BAR_REG_ADR 0xF4
#define BAR_TEMP_COMAND 0x2E
#define BAR_PRESS_COMAND 0x34

//Barometer calibration values
int16_t cAC1_3[3];
uint16_t cAC4_6[3];
int16_t cB1_2[2];
int16_t cMB_D[3];
int32_t cB5 = 2399;
int32_t initialPressure;

//Baromter calibration addresses start
#define CAC1_ADR 0xAA
#define CAC4_ADR 0xB0
#define CB1_ADR 0xB6
#define CMB_ADR 0xBA

void init_sensors(I2C_HandleTypeDef *hi2c){
	i2c = hi2c;
	
	initGyro();
	initAccelorometer();
	initCompass();
	initBaromoter();
	
	getTemp();
	initialPressure = getPres(3);
	printf("initalPres: %i\n",initialPressure);
}

void getMotion(struct sensor_MotionDef *motion){

  //Acc
  double xAcc, yAcc, zAcc;
  getAccXYZ(&xAcc, &yAcc, &zAcc);
  xAcc += xAccOff;
  yAcc += yAccOff;
  zAcc += zAccOff;
  
  double phi = atan2(yAcc,zAcc);
  double theta = -atan2(xAcc,zAcc);
  
  //Compass
  int16_t xCom, yCom, zCom;
  getComXYZ(&xCom, &yCom, &zCom);
  xCom += xComOff;
  yCom += yComOff;
  zCom += zComOff;
  int xc = xCom * gScale;
  int yc = yCom * gScale;
  int zc = zCom * gScale;
  
  double x_psi = xc*cos(theta) + zc*sin(theta);
  double y_psi = xc*sin(phi)*sin(theta) + yc*cos(phi) - zc*sin(phi)*cos(theta);
  double psi = atan2(x_psi, y_psi);
  
  if (psi < 0) psi += 2*PI;
  if (psi > 2*PI) psi -= 2*PI;
	
	//Gyro
  double Dphi, Dtheta, Dpsi;
  getGyrXYZ(&Dphi, &Dtheta, &Dpsi);
	
	motion->phi = phi;
	motion->theta = theta;
	motion->psi = psi;
	motion->Dphi = Dphi * PI/180.0;
	motion->Dtheta = Dtheta * PI/180.0;
	motion->Dpsi = Dpsi * PI/180.0;
}


/*---------------------------- Gyreoscope Methods -------------------*/
void initGyro(){
	
	uint8_t toTransmit[] = {
		L3G4200D_CTRL_REG1,
		0x0F}; 									// Normal power mode, all axes enabled
	
	HAL_I2C_Master_Transmit(i2c,GYR_ADDRESS_W,toTransmit,2,100);
}

void getGyrXYZ(double *xGyr, double *yGyr, double *zGyr){
  uint8_t rawGyr[6];
	
	uint8_t toTransmit[] = {
	L3G4200D_OUT_X_L | (1 << 7)};
		
  HAL_I2C_Master_Transmit(i2c,GYR_ADDRESS_W,toTransmit,1,100);
	HAL_I2C_Master_Receive(i2c,GYR_ADDRESS_R,(uint8_t*)rawGyr,6,100);
	
  *xGyr = ((int16_t)(rawGyr[1] << 8) | rawGyr[0]) * toDps;
  *yGyr = ((int16_t)(rawGyr[3] << 8) | rawGyr[2]) * toDps;
  *zGyr = ((int16_t)(rawGyr[5] << 8) | rawGyr[4]) * toDps;
}

HAL_StatusTypeDef isGyroReady(){
	return HAL_I2C_IsDeviceReady(i2c,GYR_ADDRESS_W,3,500);
}

/*---------------------------- Accelerometer Methods ----------------*/
void initAccelorometer(){
	
	uint8_t toTransmit[] = {
	ACC_PWR_CTL,
	0x08}; 									//Turn on
	HAL_I2C_Master_Transmit(i2c,ACC_ADDRESS_W,toTransmit,2,100);
}

void getAccXYZ(double *xAcc, double *yAcc, double *zAcc){
  uint8_t rawAcc[6];
  
	uint8_t toTransmit[] = {
	ACC_DATA_REG};
	
	HAL_I2C_Master_Transmit(i2c,ACC_ADDRESS_W,toTransmit,1,100);
	HAL_I2C_Master_Receive(i2c,ACC_ADDRESS_R,(uint8_t*)rawAcc,6,100);
  
  
  *xAcc = ((int16_t)((rawAcc[1] << 8) | rawAcc[0]) + 20) / 256.0;
  *yAcc = ((int16_t)((rawAcc[3] << 8) | rawAcc[2]) - 15) / 256.0;
  *zAcc = ((int16_t)((rawAcc[5] << 8) | rawAcc[4]) + 23) / 256.0;
}

HAL_StatusTypeDef isAccReady(){
	return HAL_I2C_IsDeviceReady(i2c,ACC_ADDRESS_W,3,500);
}

/*---------------------------- Compass Methods ----------------------*/
void initCompass(){
	
	uint8_t toTransmit[] = {
	COM_CONFIG_A,
	0x78}; // 8 samples per output, 75Hz data output rate
	HAL_I2C_Master_Transmit(i2c,COM_ADDRESS_W,toTransmit,2,100);
	
	toTransmit[0] = COM_CONFIG_B;
	toTransmit[1] = 0x20; // GAIN = +/- 1.3Ga, adjust if noisy signal
	HAL_I2C_Master_Transmit(i2c,COM_ADDRESS_W,toTransmit,2,100);
  
	toTransmit[0] = COM_MODE_REG;
	toTransmit[1] = 0x0; // NOT high speed I2C, continuous mesurement mode
	HAL_I2C_Master_Transmit(i2c,COM_ADDRESS_W,toTransmit,2,100);
}

void getComXYZ(int16_t *xCom, int16_t *yCom, int16_t *zCom){
  uint8_t rawCom[6];
	
	uint8_t toTransmit[] = {
	COM_DATA_REG};
	
	HAL_I2C_Master_Transmit(i2c,COM_ADDRESS_W,toTransmit,1,100);
	HAL_I2C_Master_Receive(i2c,COM_ADDRESS_R,(uint8_t*)rawCom,6,100);
  
  *xCom = ((rawCom[0] << 8) | rawCom[1]);
  *zCom = ((rawCom[2] << 8) | rawCom[3]);
  *yCom = ((rawCom[4] << 8) | rawCom[5]);
}

HAL_StatusTypeDef isComReady(){
	return HAL_I2C_IsDeviceReady(i2c,COM_ADDRESS_W,3,500);
}


/*---------------------------- Barometer Methods --------------------*/
void initBaromoter(){
	uint8_t barBuf[22];
  
	uint8_t toTransmit[] = {
	CAC1_ADR};

	HAL_I2C_Master_Transmit(i2c,BAR_ADDRESS_W,toTransmit,1,100);
	HAL_I2C_Master_Receive(i2c,BAR_ADDRESS_R,(uint8_t*)barBuf,22,100);
  
  cAC1_3[0] = ((int16_t)barBuf[0] << 8) + barBuf[1];
  cAC1_3[1] = ((int16_t)barBuf[2] << 8) + barBuf[3];
  cAC1_3[2] = ((int16_t)barBuf[4] << 8) + barBuf[5];
  
  cAC4_6[0] = ((uint16_t)barBuf[6] << 8) + barBuf[7];
  cAC4_6[1] = ((uint16_t)barBuf[8] << 8) + barBuf[9];
  cAC4_6[2] = ((uint16_t)barBuf[10] << 8) + barBuf[11];
  
  cB1_2[0] = ((int16_t)barBuf[12] << 8) + barBuf[13];
  cB1_2[1] = ((int16_t)barBuf[14] << 8) + barBuf[15];
  
  cMB_D[0] = ((int16_t)barBuf[16] << 8) + barBuf[17];
  cMB_D[1] = ((int16_t)barBuf[18] << 8) + barBuf[19];
  cMB_D[2] = ((int16_t)barBuf[20] << 8) + barBuf[21];
}

int32_t getPres(uint8_t oss/*Oversampling var, 0..3, (1..8 samples)*/){
  
	uint8_t toTransmit[] = {
	BAR_REG_ADR,
	BAR_PRESS_COMAND + (oss << 6)};
	
	HAL_I2C_Master_Transmit(i2c,BAR_ADDRESS_W,toTransmit,2,100);
  
	HAL_Delay(5 + 7 * oss); //Delay
  
	uint8_t barBuf[3];
	
	toTransmit[0] = BAR_DATA_REG;
	HAL_I2C_Master_Transmit(i2c,BAR_ADDRESS_W,toTransmit,2,100);
	HAL_I2C_Master_Receive(i2c,BAR_ADDRESS_R,(uint8_t*)barBuf,3,100);
	
  int32_t unPres = (((int32_t)barBuf[0] << 16) + ((int32_t)barBuf[1] << 8) + (int32_t)barBuf[2]) >> (8 - oss);
  
  int32_t barB6, barX1, barX2, barX3, barB3, pressure;
  uint32_t barB4, barB7;
  
  barB6 = cB5 - 4000;
  barX1 = ((int32_t)cB1_2[1] * ((barB6 * barB6) >> 12)) >> 11;
  barX2 = ((int32_t)cAC1_3[1] * barB6) >> 11;
  barX3 = barX1 + barX2;
  barB3 = ((((int32_t)cAC1_3[0] * 4 + barX3) << oss) + 2) >> 2;
  barX1 = ((int32_t)cAC1_3[2] * barB6) >> 13;
  barX2 = ((int32_t)cB1_2[0] * ((barB6 * barB6) >> 12)) >> 16;
  barX3 = ((barX1 + barX2) + 2) >> 2;
  barB4 = ((uint32_t)cAC4_6[0] * (uint32_t)(barX3 + 32768)) >> 15;
  barB7 = ((uint32_t)unPres - barB3) * (uint32_t)(50000 >> oss);
  if (barB7 < 0x80000000) {
      pressure = (barB7 << 1) / barB4;
  } else {
      pressure = (barB7 / barB4) << 1;
  }
  barX1 = (pressure >> 8) * (pressure >> 8);
  barX1 = (barX1 * 3038) >> 16;
  barX2 = (-7357 * pressure) >> 16;
  
  return pressure + ((barX1 + barX2 + (int32_t)3791) >> 4);
}

double getAlt(uint8_t oss/*Oversampling var, 0..3, (1..8 samples)*/){
	int32_t pressure = getPres(oss);
  return 44330 * (1.0 - pow((double)pressure / (double)initialPressure, 0.1903));

}

double getTemp(){
	
	uint8_t toTransmit[] = {
	BAR_REG_ADR,
	BAR_TEMP_COMAND};
	HAL_I2C_Master_Transmit(i2c,BAR_ADDRESS_W,toTransmit,2,100);
  
  HAL_Delay(5); //Delay
  
	toTransmit[0] = BAR_DATA_REG;
	HAL_I2C_Master_Transmit(i2c,BAR_ADDRESS_W,toTransmit,2,100);
  
  uint8_t barBuf[2];
	HAL_I2C_Master_Receive(i2c,BAR_ADDRESS_R,(uint8_t*)barBuf,2,100);
	
	int32_t unTemp;
  unTemp = ((int32_t)barBuf[0] << 8) | barBuf[1];

  int32_t barX1, barX2, temperature;
  
  barX1 = (unTemp - (int32_t)cAC4_6[2]) * (int32_t)cAC4_6[1] >> 15;
  barX2 = ((int32_t)cMB_D[1] << 11)/(barX1 + cMB_D[2]);
  cB5 = barX1 + barX2;
  temperature = (cB5 + 8) >> 4;
  
  return (double)temperature/10.0;
}

HAL_StatusTypeDef isBarReady(){
	return HAL_I2C_IsDeviceReady(i2c,BAR_ADDRESS_W,3,500);
}
