clear

%% Rotation matrix

phi = sym('phi');
theta = sym('theta');
psi = sym('psi');

R_x(phi) = [1 0 0; 0 cos(phi) -sin(phi); 0 sin(phi) cos(phi)];
R_y(theta) = [cos(theta) 0 sin(theta); 0 1 0; -sin(theta) 0 cos(theta)];
R_z(psi) = [cos(psi) -sin(psi) 0; sin(psi) cos(psi) 0; 0 0 1];

Ri_b(phi, theta, psi) = R_z(psi)*R_x(phi)*R_y(theta);
Rb_i(phi, theta, psi) = Ri_b(phi, theta, psi)';
%test
%    x  y     z   x  y  z
Ri_b(0, pi/2, 0)*[1; 0; 0];
Rb_i(0, pi/2, 0)*[0; 0; -1];
%% Euler Angles p246
Dphi = sym('Dphi');
Dtheta = sym('Dtheta');
Dpsi = sym('Dpsi');

%wi_ib = [0; 0; Dpsi] + R_z(psi)*[Dphi; 0; 0] + R_z(psi)*R_x(phi)*[0; Dtheta; 0]
wb_ib = R_y(-theta)*R_x(-phi)*[0; 0; Dpsi] + R_y(-theta)*[Dphi; 0; 0] + [0; Dtheta; 0];
%E_i(psi, phi, theta) = [0 cos(psi) -cos(phi)*sin(psi); 0 sin(psi) cos(phi)*cos(psi); 1 0 sin(phi)]
% OLD!! E_b(phi, theta, psi) = [-cos(phi)*sin(theta) cos(theta) 0; sin(phi) 0 1; cos(phi)*cos(theta) sin(theta) 0];
E_b(phi, theta, psi) = [cos(theta) 0 -cos(phi)*sin(theta); 0 1 sin(phi); sin(theta) 0 cos(phi)*cos(theta)];

DPhi = [Dphi; Dtheta; Dpsi];

wb_ib = E_b(phi, theta, psi)*DPhi;

wb_ib = sym('wb_ib',[3,1]);
DPhi = inv(E_b(psi, phi, theta))*wb_ib;

% test
%              x  y  z       x y z
DPhi = inv(E_b(0, pi/2, 0))*[0;1;0]; %OK!
vpa(DPhi);

%% Equation of motion for a rigid body p269

% Constants
m = 0.970;          %Total mass [kg]
m_p = 0.108;        %Mass of motor and propellar [kg]
m_c = m - 4*m_p;    %Center mass [kg]

L = 0.226;      %Length from center mass to out propellers, and outer masses
g = 9.81;       %Gravity [m/s^2]

% Intertia matrix p14 (quadrotor paper), p274, wikipedia; List of interta
% Point mass at a distance, cylinder
r_cylinder = 0.06;
h_cylinder = 0.05;
I_x = (L/sqrt(2))^2*m_p*4 + 1/12*m_c*(3*r_cylinder^2 + h_cylinder^2);
I_y = I_x;
I_z = L^2*m_p*4; + (m_c*r_cylinder^2)/2;

M_c = [I_x 0 0; 0 I_y 0; 0 0 I_z]

% F_bc = Rb_i(phi, theta, psi)*[0; 0; -(m_c+4*m_p)*g] + [0; 0; F_fl+F_fr+F_rl+F_rr];
% T_bc = [L/sqrt(2)*(F_fl+F_fr-F_rl-F_rr); L/sqrt(2)*(F_fl+F_rl-F_fr-F_rr); T_fl+T_rr-T_fr-T_rl];

%% Motor model (Force and Torque paramters)

u_offset = 100;
u_max = 1000;
u_hover = 338;
u_torque = 200;
t_tau = 0.2;

t_torque_ccw = (2.73 + 2.66)/2;
t_torque_cw = (3.14 + 3.12)/2;

T_c_ccw = (2*pi/t_torque_ccw^2)*I_z;       %CCW torque parameter
T_c_cw = (2*pi/t_torque_cw^2)*I_z;          %CW torque parameter
T_c = (T_c_ccw + T_c_cw)/2                   %Average torque parameter
F_c = m*g/4                                   %Force parameter

%% Controller
dt = 0.01;

% Roll
rP = 1;
rI = 1;
rD = 1;

% Pitch
pP = 1;
pI = 1;
pD = 1;

% Yaw
yP = 1;
yI = 1;
yD = 1;

%% Kalman Filter

% Noise variance
sigma_mx = 1;
sigma_my = 1;
sigma_mz = 1;
sigma_sx = 1;
sigma_sy = 1;
sigma_sz = 1;

A_k = [1 dt 0 0 0 0; zeros(1,6); 0 0 1 dt 0 0; zeros(1,6); 0 0 0 0 1 dt; zeros(1,6)];
B_k = [0 0 0; 1 0 0; 0 0 0; 0 1 0; 0 0 0; 0 0 1];
H_k = [1 0 1 0 1 0];
P_prior_0 = zeros(6,6);
x_hat_prior_0 = [0 0 0 0 0 0]';
R_k = diag([sigma_mx 0 sigma_my 0 sigma_mz 0]);
Q_k = diag([sigma_sx 0 sigma_sy 0 sigma_sz 0]);
