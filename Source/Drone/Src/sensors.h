
#ifndef __sensors_H
#define __sensors_H

#include "stm32f4xx_hal.h"
#include <math.h>

#define PI 3.14159265

struct sensor_MotionDef
{
	double phi;
	double theta;
	double psi;
	double Dphi;
	double Dtheta;
	double Dpsi;
};


void init_sensors(I2C_HandleTypeDef *hi2c);
void getMotion(struct sensor_MotionDef *motion);

/*---------------------------- Gyreoscope Methods -------------------*/
void initGyro(void);
void getGyrXYZ(double *xGyr, double *yGyr, double *zGyr);
HAL_StatusTypeDef isGyroReady(void);

/*---------------------------- Accelerometer Methods ----------------*/
void initAccelorometer(void);
void getAccXYZ(double *xAcc, double *yAcc, double *zAcc);
HAL_StatusTypeDef isAccReady(void);

/*---------------------------- Compass Methods ----------------------*/
void initCompass(void);
void getComXYZ(int16_t *xCom, int16_t *yCom, int16_t *zCom);
HAL_StatusTypeDef isComReady(void);

/*---------------------------- Barometer Methods --------------------*/
void initBaromoter(void);
int32_t getPres(uint8_t oss);
double getAlt(uint8_t oss);
double getTemp(void);
HAL_StatusTypeDef isBarReady(void);

#endif
