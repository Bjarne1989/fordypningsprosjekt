function [sys,x0,str,ts] = DiscKal(t,x,u,flag) %DiscKal(t,x,u,flag,data) if method 2 is used
% Shell for the discrete kalman filter assignment in
% TTK4115 Linear Systems.
%
% Author: J�rgen Spj�tvold
% 19/10-2003 
%

switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;%mdlInitializeSizes(data); if method 2 is used

  %%%%%%%%%%%%%
  % Outputs   %
  %%%%%%%%%%%%%
  
  case 3,
    sys=mdlOutputs(t,x,u); % mdlOutputs(t,x,u,data) if mathod 2 is used
    
  %%%%%%%%%%%%%
  % Terminate %
  %%%%%%%%%%%%%
  case 2,
    sys=mdlUpdate(t,x,u); %mdlUpdate(t,x,u, data); if method 2 is used
  
  case {1,4,}
    sys=[];

  case 9,
      sys=mdlTerminate(t,x,u);
  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error(['Unhandled flag = ',num2str(flag)]);

end

function [sys,x0,str,ts]=mdlInitializeSizes %mdlInitializeSizes(data); if method 2 is used
% This is called only at the start of the simulation. 

sizes = simsizes; % do not modify

sizes.NumContStates  = 0; % Number of continuous states in the system, do not modify
sizes.NumDiscStates  = 12; % Number of discrete states in the system, modify. 
sizes.NumOutputs     = 6; % Number of outputs
sizes.NumInputs      = 6; % Number of inputs
sizes.DirFeedthrough = 1; % 1 if the input is needed directly in the update part
sizes.NumSampleTimes = 1; % Do not modify  
sys = simsizes(sizes); % Do not modify  
str = []; % Do not modify
ts  = [-1 0]; % Sample time. [-1 0] means that sampling is
% inherited from the driving block and that it changes during
% minor steps.
global x_estimate x_prior_estimate K P_prior;
x0 = [0 0 0 0 0 0 0 0 0 0 0 0]';
sigma_sx = 1.3740;
sigma_sDx = 0.0068;
sigma_sy = 1.2249;
sigma_sDy = 0.1481;
sigma_sz = 3.5979;
sigma_sDz = 0.0098;
P_prior_0 = diag([sigma_sx sigma_sDx sigma_sy sigma_sDy sigma_sz sigma_sDz]);
x_hat_prior_0 = [0 0 0 0 0 0]';


x_prior_estimate = x_hat_prior_0;
x_estimate = x_hat_prior_0;
K = x_hat_prior_0;
P_prior = P_prior_0;


function sys=mdlUpdate(t,x,u)%,%mdlUpdate(t,x,u, data); if method 2 is used
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the filter covariance matrix and state etsimates here.
% example: sys=x+u(1), means that the state vector after
% the update equals the previous state vector + input nr one.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global x_estimate x_prior_estimate K R P_prior;
sigma_sx = 1.3740;
sigma_sDx = 0.0068;
sigma_sy = 1.2249;
sigma_sDy = 0.1481;
sigma_sz = 3.5979;
sigma_sDz = 0.0098;

sigma_mx = 1.2213e-05;
sigma_mDx = 5.8190e-04;
sigma_my = 1.1969e-05;
sigma_mDy = 0.0012;
sigma_mz = 7.2824e-05
sigma_mDz = 6.6583e-04;
dt = 0.01;
A = [1 dt 0 0 0 0; 0 1 0 0 0 0; 0 0 1 dt 0 0; 0 0 0 1 0 0; 0 0 0 0 1 dt; 0 0 0 0 0 1];%[1 dt 0 0 0 0; zeros(1,6); 0 0 1 dt 0 0; zeros(1,6); 0 0 0 0 1 dt; zeros(1,6)];
B = [0 0 0; 1 0 0; 0 0 0; 0 1 0; 0 0 0; 0 0 1];
Q = diag([sigma_sx sigma_sDx sigma_sy sigma_sDy sigma_sz sigma_sDz]);
H = [1 1 1 1 1 1];
R = diag([sigma_mx sigma_mDx sigma_my sigma_mDy sigma_mz sigma_mDz]);

P = (eye(6,6)-K*H)*P_prior; %P_prior - K*H*P_prior - P_prior*H'*K' + K*(H*P_prior*H' + H*R*H')*K'%3
% x_estimate(2) = u(2);
% x_estimate(4) = u(4);
% x_estimate(6) = u(6);
x_prior_estimate = A * x_estimate;% + B*[u(2) u(4) u(6)]';%4
P_prior = A * P * A' + Q %4

sys=[0 0 0 0 0 0 0 0 0 0 0 0];

function sys=mdlOutputs(t,x,u)% mdlOutputs(t,x,u,data) if mathod 2 is used
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the outputs here
% example: sys=x(1)+u(2), means that the output is the first state+
% the second input. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global x_estimate x_prior_estimate K R P_prior;
H = [1 1 1 1 1 1];

K = P_prior*H'*(H*P_prior*H' + H*R*H')^-1; % 1
x_estimate = x_prior_estimate + K*(H*u - H * x_prior_estimate); % 2%x_estimate = x_prior_estimate + K*(u(1) - H * x_prior_estimate); % 2

sys = x_estimate;

function sys=mdlTerminate(t,x,u)
sys = [];


