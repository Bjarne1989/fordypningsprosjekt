<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.4">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="bjarne">
<packages>
<package name="CORE407SOCKET">
<pad name="1" x="-22.86" y="30.48" drill="0.8" diameter="1.7"/>
<pad name="2" x="-20.32" y="30.48" drill="0.8" diameter="1.7"/>
<pad name="4" x="-20.32" y="27.94" drill="0.8" diameter="1.7"/>
<pad name="3" x="-22.86" y="27.94" drill="0.8" diameter="1.7"/>
<pad name="5" x="-22.86" y="25.4" drill="0.8" diameter="1.7"/>
<pad name="6" x="-20.32" y="25.4" drill="0.8" diameter="1.7"/>
<pad name="8" x="-20.32" y="22.86" drill="0.8" diameter="1.7"/>
<pad name="7" x="-22.86" y="22.86" drill="0.8" diameter="1.7"/>
<pad name="9" x="-22.86" y="20.32" drill="0.8" diameter="1.7"/>
<pad name="10" x="-20.32" y="20.32" drill="0.8" diameter="1.7"/>
<pad name="11" x="-22.86" y="17.78" drill="0.8" diameter="1.7"/>
<pad name="13" x="-22.86" y="15.24" drill="0.8" diameter="1.7"/>
<pad name="17" x="-22.86" y="10.16" drill="0.8" diameter="1.7"/>
<pad name="19" x="-22.86" y="7.62" drill="0.8" diameter="1.7"/>
<pad name="21" x="-22.86" y="5.08" drill="0.8" diameter="1.7"/>
<pad name="22" x="-20.32" y="5.08" drill="0.8" diameter="1.7"/>
<pad name="20" x="-20.32" y="7.62" drill="0.8" diameter="1.7"/>
<pad name="18" x="-20.32" y="10.16" drill="0.8" diameter="1.7"/>
<pad name="14" x="-20.32" y="15.24" drill="0.8" diameter="1.7"/>
<pad name="12" x="-20.32" y="17.78" drill="0.8" diameter="1.7"/>
<pad name="23" x="-22.86" y="2.54" drill="0.8" diameter="1.7"/>
<pad name="25" x="-22.86" y="0" drill="0.8" diameter="1.7"/>
<pad name="27" x="-22.86" y="-2.54" drill="0.8" diameter="1.7"/>
<pad name="29" x="-22.86" y="-5.08" drill="0.8" diameter="1.7"/>
<pad name="31" x="-22.86" y="-7.62" drill="0.8" diameter="1.7"/>
<pad name="33" x="-22.86" y="-10.16" drill="0.8" diameter="1.7"/>
<pad name="35" x="-22.86" y="-12.7" drill="0.8" diameter="1.7"/>
<pad name="37" x="-22.86" y="-15.24" drill="0.8" diameter="1.7"/>
<pad name="39" x="-22.86" y="-17.78" drill="0.8" diameter="1.7"/>
<pad name="41" x="-22.86" y="-20.32" drill="0.8" diameter="1.7"/>
<pad name="43" x="-22.86" y="-22.86" drill="0.8" diameter="1.7"/>
<pad name="45" x="-22.86" y="-25.4" drill="0.8" diameter="1.7"/>
<pad name="47" x="-22.86" y="-27.94" drill="0.8" diameter="1.7"/>
<pad name="49" x="-22.86" y="-30.48" drill="0.8" diameter="1.7"/>
<pad name="15" x="-22.86" y="12.7" drill="0.8" diameter="1.7"/>
<pad name="24" x="-20.32" y="2.54" drill="0.8" diameter="1.7"/>
<pad name="26" x="-20.32" y="0" drill="0.8" diameter="1.7"/>
<pad name="28" x="-20.32" y="-2.54" drill="0.8" diameter="1.7"/>
<pad name="30" x="-20.32" y="-5.08" drill="0.8" diameter="1.7"/>
<pad name="32" x="-20.32" y="-7.62" drill="0.8" diameter="1.7"/>
<pad name="34" x="-20.32" y="-10.16" drill="0.8" diameter="1.7"/>
<pad name="36" x="-20.32" y="-12.7" drill="0.8" diameter="1.7"/>
<pad name="38" x="-20.32" y="-15.24" drill="0.8" diameter="1.7"/>
<pad name="40" x="-20.32" y="-17.78" drill="0.8" diameter="1.7"/>
<pad name="42" x="-20.32" y="-20.32" drill="0.8" diameter="1.7"/>
<pad name="44" x="-20.32" y="-22.86" drill="0.8" diameter="1.7"/>
<pad name="46" x="-20.32" y="-25.4" drill="0.8" diameter="1.7"/>
<pad name="48" x="-20.32" y="-27.94" drill="0.8" diameter="1.7"/>
<pad name="50" x="-20.32" y="-30.48" drill="0.8" diameter="1.7"/>
<pad name="16" x="-20.32" y="12.7" drill="0.8" diameter="1.7"/>
<pad name="100" x="20.32" y="30.48" drill="0.8" diameter="1.7"/>
<pad name="99" x="22.86" y="30.48" drill="0.8" diameter="1.7"/>
<pad name="97" x="22.86" y="27.94" drill="0.8" diameter="1.7"/>
<pad name="98" x="20.32" y="27.94" drill="0.8" diameter="1.7"/>
<pad name="96" x="20.32" y="25.4" drill="0.8" diameter="1.7"/>
<pad name="95" x="22.86" y="25.4" drill="0.8" diameter="1.7"/>
<pad name="93" x="22.86" y="22.86" drill="0.8" diameter="1.7"/>
<pad name="94" x="20.32" y="22.86" drill="0.8" diameter="1.7"/>
<pad name="92" x="20.32" y="20.32" drill="0.8" diameter="1.7"/>
<pad name="91" x="22.86" y="20.32" drill="0.8" diameter="1.7"/>
<pad name="90" x="20.32" y="17.78" drill="0.8" diameter="1.7"/>
<pad name="88" x="20.32" y="15.24" drill="0.8" diameter="1.7"/>
<pad name="86" x="20.32" y="12.7" drill="0.8" diameter="1.7"/>
<pad name="84" x="20.32" y="10.16" drill="0.8" diameter="1.7"/>
<pad name="82" x="20.32" y="7.62" drill="0.8" diameter="1.7"/>
<pad name="81" x="22.86" y="7.62" drill="0.8" diameter="1.7"/>
<pad name="83" x="22.86" y="10.16" drill="0.8" diameter="1.7"/>
<pad name="85" x="22.86" y="12.7" drill="0.8" diameter="1.7"/>
<pad name="87" x="22.86" y="15.24" drill="0.8" diameter="1.7"/>
<pad name="89" x="22.86" y="17.78" drill="0.8" diameter="1.7"/>
<pad name="80" x="20.32" y="5.08" drill="0.8" diameter="1.7"/>
<pad name="78" x="20.32" y="2.54" drill="0.8" diameter="1.7"/>
<pad name="76" x="20.32" y="0" drill="0.8" diameter="1.7"/>
<pad name="74" x="20.32" y="-2.54" drill="0.8" diameter="1.7"/>
<pad name="72" x="20.32" y="-5.08" drill="0.8" diameter="1.7"/>
<pad name="70" x="20.32" y="-7.62" drill="0.8" diameter="1.7"/>
<pad name="68" x="20.32" y="-10.16" drill="0.8" diameter="1.7"/>
<pad name="66" x="20.32" y="-12.7" drill="0.8" diameter="1.7"/>
<pad name="62" x="20.32" y="-17.78" drill="0.8" diameter="1.7"/>
<pad name="60" x="20.32" y="-20.32" drill="0.8" diameter="1.7"/>
<pad name="58" x="20.32" y="-22.86" drill="0.8" diameter="1.7"/>
<pad name="56" x="20.32" y="-25.4" drill="0.8" diameter="1.7"/>
<pad name="54" x="20.32" y="-27.94" drill="0.8" diameter="1.7"/>
<pad name="52" x="20.32" y="-30.48" drill="0.8" diameter="1.7"/>
<pad name="64" x="20.32" y="-15.24" drill="0.8" diameter="1.7"/>
<pad name="79" x="22.86" y="5.08" drill="0.8" diameter="1.7"/>
<pad name="77" x="22.86" y="2.54" drill="0.8" diameter="1.7"/>
<pad name="75" x="22.86" y="0" drill="0.8" diameter="1.7"/>
<pad name="73" x="22.86" y="-2.54" drill="0.8" diameter="1.7"/>
<pad name="71" x="22.86" y="-5.08" drill="0.8" diameter="1.7"/>
<pad name="69" x="22.86" y="-7.62" drill="0.8" diameter="1.7"/>
<pad name="67" x="22.86" y="-10.16" drill="0.8" diameter="1.7"/>
<pad name="65" x="22.86" y="-12.7" drill="0.8" diameter="1.7"/>
<pad name="61" x="22.86" y="-17.78" drill="0.8" diameter="1.7"/>
<pad name="59" x="22.86" y="-20.32" drill="0.8" diameter="1.7"/>
<pad name="57" x="22.86" y="-22.86" drill="0.8" diameter="1.7"/>
<pad name="55" x="22.86" y="-25.4" drill="0.8" diameter="1.7"/>
<pad name="53" x="22.86" y="-27.94" drill="0.8" diameter="1.7"/>
<pad name="51" x="22.86" y="-30.48" drill="0.8" diameter="1.7"/>
<pad name="63" x="22.86" y="-15.24" drill="0.8" diameter="1.7"/>
<wire x1="-27.94" y1="35.56" x2="27.94" y2="35.56" width="0.127" layer="21"/>
<wire x1="27.94" y1="35.56" x2="27.94" y2="-33.02" width="0.127" layer="21"/>
<wire x1="27.94" y1="-33.02" x2="-27.94" y2="-33.02" width="0.127" layer="21"/>
<wire x1="-27.94" y1="-33.02" x2="-27.94" y2="35.56" width="0.127" layer="21"/>
<wire x1="16.51" y1="-30.48" x2="16.51" y2="-22.86" width="0.127" layer="21"/>
<wire x1="16.51" y1="-22.86" x2="-16.51" y2="-22.86" width="0.127" layer="21"/>
<wire x1="-16.51" y1="-22.86" x2="-16.51" y2="-31.75" width="0.127" layer="21"/>
<wire x1="-16.51" y1="-31.75" x2="-2.54" y2="-31.75" width="0.127" layer="21"/>
<wire x1="2.54" y1="-31.75" x2="16.51" y2="-31.75" width="0.127" layer="21"/>
<wire x1="16.51" y1="-31.75" x2="16.51" y2="-30.48" width="0.127" layer="21"/>
<text x="-6.35" y="-22.86" size="1.778" layer="21">JTAG/SWD</text>
<text x="-11.43" y="-3.81" size="3.81" layer="21">  STM32
Core407V</text>
</package>
<package name="GY80">
<pad name="1" x="-5.334" y="11.938" drill="0.9" diameter="1.7"/>
<pad name="2" x="-5.334" y="9.398" drill="0.9" diameter="1.7"/>
<pad name="3" x="-5.334" y="6.858" drill="0.9" diameter="1.7"/>
<pad name="4" x="-5.334" y="4.318" drill="0.9" diameter="1.7"/>
<pad name="5" x="-5.334" y="1.778" drill="0.9" diameter="1.7"/>
<pad name="6" x="-5.334" y="-0.762" drill="0.9" diameter="1.7"/>
<pad name="7" x="-5.334" y="-3.302" drill="0.9" diameter="1.7"/>
<pad name="8" x="-5.334" y="-5.842" drill="0.9" diameter="1.7"/>
<pad name="9" x="-5.334" y="-8.382" drill="0.9" diameter="1.7"/>
<pad name="10" x="-5.334" y="-10.922" drill="0.9" diameter="1.7"/>
<wire x1="-6.858" y1="13.716" x2="-6.858" y2="-12.7" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-12.7" x2="10.16" y2="-12.7" width="0.127" layer="21"/>
<wire x1="10.16" y1="-12.7" x2="10.16" y2="13.716" width="0.127" layer="21"/>
<wire x1="10.16" y1="13.716" x2="-6.858" y2="13.716" width="0.127" layer="21"/>
<hole x="7.112" y="10.668" drill="3.5"/>
<hole x="7.112" y="-9.652" drill="3.5"/>
<text x="2.54" y="-2.54" size="1.778" layer="21" rot="R90">GY-80</text>
</package>
</packages>
<symbols>
<symbol name="CORE407V">
<wire x1="-38.1" y1="38.1" x2="-38.1" y2="-38.1" width="0.254" layer="94"/>
<wire x1="-38.1" y1="-38.1" x2="38.1" y2="-38.1" width="0.254" layer="94"/>
<wire x1="38.1" y1="-38.1" x2="38.1" y2="38.1" width="0.254" layer="94"/>
<wire x1="38.1" y1="38.1" x2="-38.1" y2="38.1" width="0.254" layer="94"/>
<text x="-15.24" y="-5.08" size="5.08" layer="94">  STM32
Core407V</text>
<pin name="PB12" x="-43.18" y="30.48" length="middle"/>
<pin name="PB13" x="-43.18" y="27.94" length="middle"/>
<pin name="PB14" x="-43.18" y="25.4" length="middle"/>
<pin name="PB15" x="-43.18" y="22.86" length="middle"/>
<pin name="PD8" x="-43.18" y="20.32" length="middle"/>
<pin name="PD9" x="-43.18" y="17.78" length="middle"/>
<pin name="PD10" x="-43.18" y="15.24" length="middle"/>
<pin name="PD11" x="-43.18" y="12.7" length="middle"/>
<pin name="PD13" x="-43.18" y="7.62" length="middle"/>
<pin name="PD14" x="-43.18" y="5.08" length="middle"/>
<pin name="PD15" x="-43.18" y="2.54" length="middle"/>
<pin name="PC6" x="-43.18" y="0" length="middle"/>
<pin name="PC7" x="-43.18" y="-2.54" length="middle"/>
<pin name="PC8" x="-43.18" y="-5.08" length="middle"/>
<pin name="PC9" x="-43.18" y="-7.62" length="middle"/>
<pin name="PA8" x="-43.18" y="-10.16" length="middle"/>
<pin name="PA9" x="-43.18" y="-12.7" length="middle"/>
<pin name="PA10" x="-43.18" y="-15.24" length="middle"/>
<pin name="PA11" x="-43.18" y="-17.78" length="middle"/>
<pin name="PA12" x="-43.18" y="-20.32" length="middle"/>
<pin name="PA13" x="-43.18" y="-22.86" length="middle"/>
<pin name="3.3V" x="-43.18" y="-25.4" length="middle"/>
<pin name="GND" x="-43.18" y="-27.94" length="middle"/>
<pin name="3.3V1" x="-43.18" y="-30.48" length="middle"/>
<pin name="PA14" x="-30.48" y="-43.18" length="middle" rot="R90"/>
<pin name="PA15" x="-27.94" y="-43.18" length="middle" rot="R90"/>
<pin name="PC10" x="-25.4" y="-43.18" length="middle" rot="R90"/>
<pin name="PC11" x="-22.86" y="-43.18" length="middle" rot="R90"/>
<pin name="PC12" x="-20.32" y="-43.18" length="middle" rot="R90"/>
<pin name="PD1" x="-15.24" y="-43.18" length="middle" rot="R90"/>
<pin name="PD2" x="-12.7" y="-43.18" length="middle" rot="R90"/>
<pin name="PD3" x="-10.16" y="-43.18" length="middle" rot="R90"/>
<pin name="PD4" x="-7.62" y="-43.18" length="middle" rot="R90"/>
<pin name="PD5" x="-5.08" y="-43.18" length="middle" rot="R90"/>
<pin name="PD6" x="-2.54" y="-43.18" length="middle" rot="R90"/>
<pin name="PD7" x="0" y="-43.18" length="middle" rot="R90"/>
<pin name="PB3" x="2.54" y="-43.18" length="middle" rot="R90"/>
<pin name="PB4" x="5.08" y="-43.18" length="middle" rot="R90"/>
<pin name="PB5" x="7.62" y="-43.18" length="middle" rot="R90"/>
<pin name="PB6" x="10.16" y="-43.18" length="middle" rot="R90"/>
<pin name="PB7" x="12.7" y="-43.18" length="middle" rot="R90"/>
<pin name="BOOT0" x="15.24" y="-43.18" length="middle" rot="R90"/>
<pin name="PB8" x="17.78" y="-43.18" length="middle" rot="R90"/>
<pin name="PB9" x="20.32" y="-43.18" length="middle" rot="R90"/>
<pin name="PE0" x="22.86" y="-43.18" length="middle" rot="R90"/>
<pin name="PE1" x="25.4" y="-43.18" length="middle" rot="R90"/>
<pin name="GND1" x="27.94" y="-43.18" length="middle" rot="R90"/>
<pin name="3.3V2" x="30.48" y="-43.18" length="middle" rot="R90"/>
<pin name="PD0" x="-17.78" y="-43.18" length="middle" rot="R90"/>
<pin name="PE2" x="43.18" y="-30.48" length="middle" rot="R180"/>
<pin name="PE3" x="43.18" y="-27.94" length="middle" rot="R180"/>
<pin name="PE4" x="43.18" y="-25.4" length="middle" rot="R180"/>
<pin name="PE5" x="43.18" y="-22.86" length="middle" rot="R180"/>
<pin name="PE6" x="43.18" y="-20.32" length="middle" rot="R180"/>
<pin name="VBAT" x="43.18" y="-17.78" length="middle" rot="R180"/>
<pin name="PC13" x="43.18" y="-15.24" length="middle" rot="R180"/>
<pin name="PC14" x="43.18" y="-12.7" length="middle" rot="R180"/>
<pin name="PC15" x="43.18" y="-10.16" length="middle" rot="R180"/>
<pin name="GND2" x="43.18" y="-7.62" length="middle" rot="R180"/>
<pin name="3.3V3" x="43.18" y="-5.08" length="middle" rot="R180"/>
<pin name="OSC_IN" x="43.18" y="-2.54" length="middle" rot="R180"/>
<pin name="OSC_O" x="43.18" y="0" length="middle" rot="R180"/>
<pin name="RESET" x="43.18" y="2.54" length="middle" rot="R180"/>
<pin name="PC0" x="43.18" y="5.08" length="middle" rot="R180"/>
<pin name="PC1" x="43.18" y="7.62" length="middle" rot="R180"/>
<pin name="PC2" x="43.18" y="10.16" length="middle" rot="R180"/>
<pin name="PC3" x="43.18" y="12.7" length="middle" rot="R180"/>
<pin name="3.3V4" x="43.18" y="15.24" length="middle" rot="R180"/>
<pin name="GND/VREF-" x="43.18" y="17.78" length="middle" rot="R180"/>
<pin name="VREF+" x="43.18" y="20.32" length="middle" rot="R180"/>
<pin name="3.3V5" x="43.18" y="22.86" length="middle" rot="R180"/>
<pin name="PA0" x="43.18" y="25.4" length="middle" rot="R180"/>
<pin name="PA1" x="43.18" y="27.94" length="middle" rot="R180"/>
<pin name="PA2" x="43.18" y="30.48" length="middle" rot="R180"/>
<pin name="PA3" x="30.48" y="43.18" length="middle" rot="R270"/>
<pin name="GND3" x="27.94" y="43.18" length="middle" rot="R270"/>
<pin name="3.3V6" x="25.4" y="43.18" length="middle" rot="R270"/>
<pin name="PA4" x="22.86" y="43.18" length="middle" rot="R270"/>
<pin name="PA5" x="20.32" y="43.18" length="middle" rot="R270"/>
<pin name="PA6" x="17.78" y="43.18" length="middle" rot="R270"/>
<pin name="PA7" x="15.24" y="43.18" length="middle" rot="R270"/>
<pin name="PC4" x="12.7" y="43.18" length="middle" rot="R270"/>
<pin name="PC5" x="10.16" y="43.18" length="middle" rot="R270"/>
<pin name="PB0" x="7.62" y="43.18" length="middle" rot="R270"/>
<pin name="PB1" x="5.08" y="43.18" length="middle" rot="R270"/>
<pin name="PB2" x="2.54" y="43.18" length="middle" rot="R270"/>
<pin name="PE7" x="0" y="43.18" length="middle" rot="R270"/>
<pin name="PE8" x="-2.54" y="43.18" length="middle" rot="R270"/>
<pin name="PE9" x="-5.08" y="43.18" length="middle" rot="R270"/>
<pin name="PE10" x="-7.62" y="43.18" length="middle" rot="R270"/>
<pin name="PE11" x="-10.16" y="43.18" length="middle" rot="R270"/>
<pin name="PE12" x="-12.7" y="43.18" length="middle" rot="R270"/>
<pin name="PE13" x="-15.24" y="43.18" length="middle" rot="R270"/>
<pin name="PE14" x="-17.78" y="43.18" length="middle" rot="R270"/>
<pin name="PE15" x="-20.32" y="43.18" length="middle" rot="R270"/>
<pin name="PB10" x="-22.86" y="43.18" length="middle" rot="R270"/>
<pin name="PB11" x="-25.4" y="43.18" length="middle" rot="R270"/>
<pin name="GND4" x="-27.94" y="43.18" length="middle" rot="R270"/>
<pin name="5V_IN" x="-30.48" y="43.18" length="middle" rot="R270"/>
<pin name="PD12" x="-43.18" y="10.16" length="middle"/>
</symbol>
<symbol name="GY80">
<wire x1="-10.16" y1="15.24" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="15.24" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<pin name="VCC_IN" x="-15.24" y="12.7" length="middle"/>
<pin name="VCC_3.3V" x="-15.24" y="10.16" length="middle"/>
<pin name="GND" x="-15.24" y="7.62" length="middle"/>
<pin name="SCL" x="-15.24" y="5.08" length="middle"/>
<pin name="SDA" x="-15.24" y="2.54" length="middle"/>
<pin name="M_DRDY" x="-15.24" y="0" length="middle"/>
<pin name="A_INT" x="-15.24" y="-2.54" length="middle"/>
<pin name="T_INT" x="-15.24" y="-5.08" length="middle"/>
<pin name="P_XCLR" x="-15.24" y="-7.62" length="middle"/>
<pin name="P_EOC" x="-15.24" y="-10.16" length="middle"/>
<text x="5.08" y="-2.54" size="1.9304" layer="94" rot="R90">GY-80</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CORE407V">
<gates>
<gate name="G$1" symbol="CORE407V" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CORE407SOCKET">
<connects>
<connect gate="G$1" pin="3.3V" pad="23"/>
<connect gate="G$1" pin="3.3V1" pad="25"/>
<connect gate="G$1" pin="3.3V2" pad="50"/>
<connect gate="G$1" pin="3.3V3" pad="61"/>
<connect gate="G$1" pin="3.3V4" pad="69"/>
<connect gate="G$1" pin="3.3V5" pad="72"/>
<connect gate="G$1" pin="3.3V6" pad="78"/>
<connect gate="G$1" pin="5V_IN" pad="100"/>
<connect gate="G$1" pin="BOOT0" pad="44"/>
<connect gate="G$1" pin="GND" pad="24"/>
<connect gate="G$1" pin="GND/VREF-" pad="70"/>
<connect gate="G$1" pin="GND1" pad="49"/>
<connect gate="G$1" pin="GND2" pad="60"/>
<connect gate="G$1" pin="GND3" pad="77"/>
<connect gate="G$1" pin="GND4" pad="99"/>
<connect gate="G$1" pin="OSC_IN" pad="62"/>
<connect gate="G$1" pin="OSC_O" pad="63"/>
<connect gate="G$1" pin="PA0" pad="73"/>
<connect gate="G$1" pin="PA1" pad="74"/>
<connect gate="G$1" pin="PA10" pad="19"/>
<connect gate="G$1" pin="PA11" pad="20"/>
<connect gate="G$1" pin="PA12" pad="21"/>
<connect gate="G$1" pin="PA13" pad="22"/>
<connect gate="G$1" pin="PA14" pad="26"/>
<connect gate="G$1" pin="PA15" pad="27"/>
<connect gate="G$1" pin="PA2" pad="75"/>
<connect gate="G$1" pin="PA3" pad="76"/>
<connect gate="G$1" pin="PA4" pad="79"/>
<connect gate="G$1" pin="PA5" pad="80"/>
<connect gate="G$1" pin="PA6" pad="81"/>
<connect gate="G$1" pin="PA7" pad="82"/>
<connect gate="G$1" pin="PA8" pad="17"/>
<connect gate="G$1" pin="PA9" pad="18"/>
<connect gate="G$1" pin="PB0" pad="85"/>
<connect gate="G$1" pin="PB1" pad="86"/>
<connect gate="G$1" pin="PB10" pad="97"/>
<connect gate="G$1" pin="PB11" pad="98"/>
<connect gate="G$1" pin="PB12" pad="1"/>
<connect gate="G$1" pin="PB13" pad="2"/>
<connect gate="G$1" pin="PB14" pad="3"/>
<connect gate="G$1" pin="PB15" pad="4"/>
<connect gate="G$1" pin="PB2" pad="87"/>
<connect gate="G$1" pin="PB3" pad="39"/>
<connect gate="G$1" pin="PB4" pad="40"/>
<connect gate="G$1" pin="PB5" pad="41"/>
<connect gate="G$1" pin="PB6" pad="42"/>
<connect gate="G$1" pin="PB7" pad="43"/>
<connect gate="G$1" pin="PB8" pad="45"/>
<connect gate="G$1" pin="PB9" pad="46"/>
<connect gate="G$1" pin="PC0" pad="65"/>
<connect gate="G$1" pin="PC1" pad="66"/>
<connect gate="G$1" pin="PC10" pad="28"/>
<connect gate="G$1" pin="PC11" pad="29"/>
<connect gate="G$1" pin="PC12" pad="30"/>
<connect gate="G$1" pin="PC13" pad="57"/>
<connect gate="G$1" pin="PC14" pad="58"/>
<connect gate="G$1" pin="PC15" pad="59"/>
<connect gate="G$1" pin="PC2" pad="67"/>
<connect gate="G$1" pin="PC3" pad="68"/>
<connect gate="G$1" pin="PC4" pad="83"/>
<connect gate="G$1" pin="PC5" pad="84"/>
<connect gate="G$1" pin="PC6" pad="13"/>
<connect gate="G$1" pin="PC7" pad="14"/>
<connect gate="G$1" pin="PC8" pad="15"/>
<connect gate="G$1" pin="PC9" pad="16"/>
<connect gate="G$1" pin="PD0" pad="31"/>
<connect gate="G$1" pin="PD1" pad="32"/>
<connect gate="G$1" pin="PD10" pad="7"/>
<connect gate="G$1" pin="PD11" pad="8"/>
<connect gate="G$1" pin="PD12" pad="9"/>
<connect gate="G$1" pin="PD13" pad="10"/>
<connect gate="G$1" pin="PD14" pad="11"/>
<connect gate="G$1" pin="PD15" pad="12"/>
<connect gate="G$1" pin="PD2" pad="33"/>
<connect gate="G$1" pin="PD3" pad="34"/>
<connect gate="G$1" pin="PD4" pad="35"/>
<connect gate="G$1" pin="PD5" pad="36"/>
<connect gate="G$1" pin="PD6" pad="37"/>
<connect gate="G$1" pin="PD7" pad="38"/>
<connect gate="G$1" pin="PD8" pad="5"/>
<connect gate="G$1" pin="PD9" pad="6"/>
<connect gate="G$1" pin="PE0" pad="47"/>
<connect gate="G$1" pin="PE1" pad="48"/>
<connect gate="G$1" pin="PE10" pad="91"/>
<connect gate="G$1" pin="PE11" pad="92"/>
<connect gate="G$1" pin="PE12" pad="93"/>
<connect gate="G$1" pin="PE13" pad="94"/>
<connect gate="G$1" pin="PE14" pad="95"/>
<connect gate="G$1" pin="PE15" pad="96"/>
<connect gate="G$1" pin="PE2" pad="51"/>
<connect gate="G$1" pin="PE3" pad="52"/>
<connect gate="G$1" pin="PE4" pad="53"/>
<connect gate="G$1" pin="PE5" pad="54"/>
<connect gate="G$1" pin="PE6" pad="55"/>
<connect gate="G$1" pin="PE7" pad="88"/>
<connect gate="G$1" pin="PE8" pad="89"/>
<connect gate="G$1" pin="PE9" pad="90"/>
<connect gate="G$1" pin="RESET" pad="64"/>
<connect gate="G$1" pin="VBAT" pad="56"/>
<connect gate="G$1" pin="VREF+" pad="71"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GY80">
<gates>
<gate name="G$1" symbol="GY80" x="0" y="0"/>
</gates>
<devices>
<device name="" package="GY80">
<connects>
<connect gate="G$1" pin="A_INT" pad="7"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="M_DRDY" pad="6"/>
<connect gate="G$1" pin="P_EOC" pad="10"/>
<connect gate="G$1" pin="P_XCLR" pad="9"/>
<connect gate="G$1" pin="SCL" pad="4"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="T_INT" pad="8"/>
<connect gate="G$1" pin="VCC_3.3V" pad="2"/>
<connect gate="G$1" pin="VCC_IN" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3L-LOC">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="bjarne" deviceset="CORE407V" device=""/>
<part name="U$2" library="bjarne" deviceset="GY80" device=""/>
<part name="FRAME1" library="frames" deviceset="A3L-LOC" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="48.26" y="38.1"/>
<instance part="U$2" gate="G$1" x="142.24" y="48.26"/>
<instance part="FRAME1" gate="G$1" x="-119.38" y="-55.88"/>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
