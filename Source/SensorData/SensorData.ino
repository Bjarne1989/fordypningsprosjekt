#include <Wire.h>
//Compass
#define COM_ADDRESS 0x1E
#define COM_CONFIG_A 0x00
#define COM_CONFIG_B 0x01
#define COM_MODE_REG 0x02
#define COM_DATA_REG 0x03

const float xOffset = -55.0 - 12.0;     // Offset required to adjust x coordinate to zero origin
const float yOffset = 100.0 + 20.0;        // Offset required to adjust y coordinate to zero origin
const float declination = 2.80;    // Enter magnetic declination mrads here (local to your geo area) 

//Accelerometer
#define ACC_DATA_REG 0x32
#define ACC_ADDRESS 0x53
#define ACC_PWR_CTL 0x2D

//Gyreoscope
#define GYR_ADDRESS (0xD2 >> 1)
#define L3G4200D_OUT_X_L 0x28
#define L3G4200D_CTRL_REG1 0x20

//Barometer
#define BAR_ADDRESS 0x77
#define BAR_DATA_REG 0xF6
#define BAR_REG_ADR 0xF4
#define BAR_TEMP_COMAND 0x2E
#define BAR_PRESS_COMAND 0x34

//Barometer calibration values
int16_t cAC1_3[3];
uint16_t cAC4_6[3];
int16_t cB1_2[2];
int16_t cMB_D[3];

int32_t cB5 = 2399;
int32_t unTemp = 150;
int32_t initialPressure;

//Baromter calibration addresses start
#define CAC1_ADR 0xAA
#define CAC4_ADR 0xB0
#define CB1_ADR 0xB6
#define CMB_ADR 0xBA

uint32_t timer;

void setup(){
  Wire.begin();
  Serial.begin(115200);
  delay(500);
  
  Serial.println("init..");
  
  //Acc
  initAccelorometer();
  delay(500);
  
  //Gyr
  initGyro();
  delay(500);
  
  //Barometer
  initBarometer();
  delay(500);
  initialPressure = getPressure(3);
  delay(500);
  
  //Compass
  initCompass();
  
  Serial.println("DONE!");
}

void loop(){
  // Accelerometer
  timer = micros();
  double xAcc, yAcc, zAcc;
  double xGyr, yGyr, zGyr;
  int xCom, yCom, zCom;
  
//  getAccXYZ(&xAcc, &yAcc, &zAcc);
//  getGyrXYZ(&xGyr, &yGyr, &zGyr);
  //Print to simulink
//  union{byte uBytes[4];double uDouble;} xAccData;
//  xAccData.uDouble = xAcc;
//  union{byte uBytes[4];double uDouble;} yAccData;
//  yAccData.uDouble = yAcc;
//  union{byte uBytes[4];double uDouble;} zAccData;
//  zAccData.uDouble = zAcc;
//  
//  union{byte uBytes[4];double uDouble;} xGyrData;
//  xGyrData.uDouble = xGyr;
//  union{byte uBytes[4];double uDouble;} yGyrData;
//  yGyrData.uDouble = yGyr;
//  union{byte uBytes[4];double uDouble;} zGyrData;
//  zGyrData.uDouble = zGyr;
//  
//  Serial.write(xAccData.uBytes, 4);
//  Serial.write(yAccData.uBytes, 4);
//  Serial.write(zAccData.uBytes, 4);
//  
//  Serial.write(xGyrData.uBytes, 4);
//  Serial.write(yGyrData.uBytes, 4);
//  Serial.write(zGyrData.uBytes, 4);Serial.write("\n");
//  
//  Serial.print("Acc: ");
//  Serial.print("X:");Serial.print(xAcc);
//  Serial.print(" Y:");Serial.print(yAcc);
//  Serial.print(" Z:");Serial.print(zAcc);
//  
//  // Compass http://forum.arduino.cc/index.php?topic=149073.0
  getComXYZ(&xCom, &yCom, &zCom);
  
  int xs, ys, zs;

  float gScale = .92;  // Scale factor for +1.3Ga setting
  
  float adjx = xCom + xOffset;
  float adjy = yCom + yOffset;

  xs = adjx * gScale; 
  ys = adjy * gScale;
  zs = zCom * gScale;
  
  float heading = atan2(ys, xs);
  heading += declination / 1000; // Declination for geo area
    
  if (heading < 0) heading += 2*PI;
    
  if (heading > 2*PI) heading -= 2*PI;

  float angle = heading * 180/M_PI;
  
//  Serial.print("X: ");
//  Serial.print(xs);
//  Serial.print("   Y: ");
//  Serial.print(ys);
//  Serial.print("   Z: ");
//  Serial.print(zs);
//  Serial.print("   Xs: ");
//  Serial.print(xs);
//  Serial.print("   Ys: ");
//  Serial.print(ys);
//  Serial.print("   Zs: ");
//  Serial.print(zs);
//  Serial.print("   H: ");
//  Serial.print(heading);
//  Serial.print("   A: ");
//  Serial.println(angle);
//  Serial.print(" Com: ");
//  Serial.print("X:");
  Serial.print(xs);
  Serial.print(":"); Serial.print(ys);
  Serial.print(":");Serial.print(zs);
  /*
  //
  // Gyroscope
  getGyrXYZ(&xGyr, &yGyr, &zGyr);
  
  Serial.print(" Gyr: ");
  Serial.print("X:");Serial.print(xGyr);
  Serial.print(" Y:"); Serial.print(yGyr);
  Serial.print(" Z:");Serial.print(zGyr);
  
  // Temperature
  float temp = getTemperature();
  Serial.print(" Temp: ");Serial.print(temp);
  
  // Height
  float alt = getAltitude(3);
  Serial.print(" Alt: ");Serial.print(alt);
  
  // Timer
  //Serial.print(" T: ");Serial.print(micros() - timer);*/
  Serial.println();

  delay(100);
}

void getBytes(int adress, uint8_t *buffer, int bytes){
  Wire.requestFrom(adress, 22);
  
  uint8_t counter = 0;  
  while ((Wire.available()) && (counter < bytes)){
    buffer[counter++] = Wire.read();
  }
}

/*
*      GYRO
*/

void initGyro(){
  Wire.beginTransmission(GYR_ADDRESS);
  Wire.write(L3G4200D_CTRL_REG1);
  Wire.write(0x0F); // Normal power mode, all axes enabled
  Wire.endTransmission();
}

void getGyrXYZ(double *xGyr, double *yGyr, double *zGyr){
  uint8_t rawGyr[6];
  
  Wire.beginTransmission(GYR_ADDRESS);
  Wire.write(L3G4200D_OUT_X_L | (1 << 7)); 
  Wire.endTransmission();
  Wire.requestFrom(GYR_ADDRESS, 6);
  
  getBytes(GYR_ADDRESS, (uint8_t*)rawGyr, 6);
  
  *xGyr = (rawGyr[0] << 8 | rawGyr[1]) * 250.0/32768;
  *yGyr = (rawGyr[2] << 8 | rawGyr[3]) * 250.0/32768;
  *zGyr = (rawGyr[4] << 8 | rawGyr[5]) * 250.0/32768;
}

/*
*      COMPASS
*/

void initCompass(){
  Wire.beginTransmission(COM_ADDRESS);
  Wire.write(COM_CONFIG_A);
  Wire.write(0b01111000); // 8 samples per output, 75Hz data output rate
  Wire.endTransmission();
  
  Wire.beginTransmission(COM_ADDRESS);
  Wire.write(COM_CONFIG_B);
  Wire.write(0b00100000); // GAIN = +/- 1.3Ga, adjust if noisy signal
  Wire.endTransmission();
  
  Wire.beginTransmission(COM_ADDRESS);
  Wire.write(COM_MODE_REG);
  Wire.write(0b00000000); // NOT high speed I2C, continuous mesurement mode
  Wire.endTransmission();
}

void getComXYZ(int *xCom, int *yCom, int *zCom){
  uint8_t rawCom[6];
  
  Wire.beginTransmission(COM_ADDRESS);
  Wire.write(COM_DATA_REG);
  Wire.endTransmission();
  
  getBytes(COM_ADDRESS, (uint8_t*)rawCom, 6);
  
  *xCom = ((rawCom[0] << 8) | rawCom[1]);
  *zCom = ((rawCom[2] << 8) | rawCom[3]);
  *yCom = ((rawCom[4] << 8) | rawCom[5]);
}

/*
*      ACCELEROMETER
*/

void initAccelorometer(){
  Wire.beginTransmission(ACC_ADDRESS);
  Wire.write(ACC_PWR_CTL); //Turn on
  Wire.write(0x08);
  Wire.endTransmission();
}

void getAccXYZ(double *xAcc, double *yAcc, double *zAcc){
  byte rawAcc[6];
  
  Wire.beginTransmission(ACC_ADDRESS);
  Wire.write(ACC_DATA_REG);
  Wire.endTransmission();
  
  getBytes(ACC_ADDRESS, (uint8_t*)rawAcc, 6);
  
  *xAcc = ((rawAcc[0] | (rawAcc[1] << 8)) + 20) / 256.0;
  *yAcc = ((rawAcc[2] | (rawAcc[3] << 8)) - 15) / 256.0;
  *zAcc = ((rawAcc[4] | (rawAcc[5] << 8)) + 23) / 256.0;
}
/*
*      BAROMETER
*/

void initBarometer(){
  uint8_t barBuf[22];
  
  Wire.beginTransmission(BAR_ADDRESS);
  Wire.write(CAC1_ADR);
  Wire.endTransmission();
  
  getBytes(BAR_ADDRESS, (uint8_t*)barBuf, 22);
  
  cAC1_3[0] = ((int16_t)barBuf[0] << 8) + barBuf[1];
  cAC1_3[1] = ((int16_t)barBuf[2] << 8) + barBuf[3];
  cAC1_3[2] = ((int16_t)barBuf[4] << 8) + barBuf[5];
  
  cAC4_6[0] = ((uint16_t)barBuf[6] << 8) + barBuf[7];
  cAC4_6[1] = ((uint16_t)barBuf[8] << 8) + barBuf[9];
  cAC4_6[2] = ((uint16_t)barBuf[10] << 8) + barBuf[11];
  
  cB1_2[0] = ((int16_t)barBuf[12] << 8) + barBuf[13];
  cB1_2[1] = ((int16_t)barBuf[14] << 8) + barBuf[15];
  
  cMB_D[0] = ((int16_t)barBuf[16] << 8) + barBuf[17];
  cMB_D[1] = ((int16_t)barBuf[18] << 8) + barBuf[19];
  cMB_D[2] = ((int16_t)barBuf[20] << 8) + barBuf[21];
  
  getTemperature();
  /*for (int i = 0; i < 3; i++){
    Serial.print(cAC1_3[i]);
    Serial.print(" ");
  }
  Serial.print("\t");
  for (int i = 0; i < 3; i++){
    Serial.print(cAC4_6[i]);
    Serial.print(" ");
  }
  Serial.print("\t");
  for (int i = 0; i < 2; i++){
    Serial.print(cB1_2[i]);
    Serial.print(" ");
  }
  Serial.print("\t");
  for (int i = 0; i < 3; i++){
    Serial.print(cMB_D[i]);
    Serial.print(" ");
  }
  Serial.println();*/
}

float getAltitude(int oss){
  uint32_t pressure = getPressure(oss);
  return 44330 * (1.0 - pow((float)pressure / (float)initialPressure, 0.1903));
}

int32_t getPressure(int oss/*Oversampling var, 0..3, (1..8 samples)*/){
  Wire.beginTransmission(BAR_ADDRESS);
  Wire.write(BAR_REG_ADR);
  Wire.write(BAR_PRESS_COMAND + (oss << 6));
  Wire.endTransmission();
  
  delay(5 + 7 * oss); //MÅÅÅÅ BORT!!!!
  
  Wire.beginTransmission(BAR_ADDRESS);
  Wire.write(BAR_DATA_REG);
  Wire.endTransmission();
  
  uint8_t barBuf[3];
  getBytes(BAR_ADDRESS, (uint8_t*)barBuf, 3);
  int32_t unPres = (((int32_t)barBuf[0] << 16) + ((int32_t)barBuf[1] << 8) + (int32_t)barBuf[2]) >> (8 - oss);
  
  int32_t barB6, barX1, barX2, barX3, barB3, pressure;
  uint32_t barB4, barB7;
  
  barB6 = cB5 - 4000;
  barX1 = ((int32_t)cB1_2[1] * ((barB6 * barB6) >> 12)) >> 11;
  barX2 = ((int32_t)cAC1_3[1] * barB6) >> 11;
  barX3 = barX1 + barX2;
  barB3 = ((((int32_t)cAC1_3[0] * 4 + barX3) << oss) + 2) >> 2;
  barX1 = ((int32_t)cAC1_3[2] * barB6) >> 13;
  barX2 = ((int32_t)cB1_2[0] * ((barB6 * barB6) >> 12)) >> 16;
  barX3 = ((barX1 + barX2) + 2) >> 2;
  barB4 = ((uint32_t)cAC4_6[0] * (uint32_t)(barX3 + 32768)) >> 15;
  barB7 = ((uint32_t)unPres - barB3) * (uint32_t)(50000 >> oss);
  if (barB7 < 0x80000000) {
      pressure = (barB7 << 1) / barB4;
  } else {
      pressure = (barB7 / barB4) << 1;
  }
  barX1 = (pressure >> 8) * (pressure >> 8);
  barX1 = (barX1 * 3038) >> 16;
  barX2 = (-7357 * pressure) >> 16;
  
  //Serial.println(pressure + ((barX1 + barX2 + (int32_t)3791) >> 4));
  
  return pressure + ((barX1 + barX2 + (int32_t)3791) >> 4);
}

float getTemperature(){
  Wire.beginTransmission(BAR_ADDRESS);
  Wire.write(BAR_REG_ADR);
  Wire.write(BAR_TEMP_COMAND);
  Wire.endTransmission();
  
  delay(5); //MÅÅÅÅ BORT!!!!
  
  Wire.beginTransmission(BAR_ADDRESS);
  Wire.write(BAR_DATA_REG);
  Wire.endTransmission();
  
  uint8_t barBuf[2];
  getBytes(BAR_ADDRESS, (uint8_t*)barBuf, 2);
  unTemp = (barBuf[0] << 8) + barBuf[1];
  
  int32_t barX1, barX2, temperature;
  
  barX1 = (unTemp - (int32_t)cAC4_6[2]) * (int32_t)cAC4_6[1] >> 15;
  barX2 = ((int32_t)cMB_D[1] << 11)/(barX1 + cMB_D[2]);
  cB5 = barX1 + barX2;
  temperature = (cB5 + 8) >> 4;
  
  return (float)temperature/10.0;
}
