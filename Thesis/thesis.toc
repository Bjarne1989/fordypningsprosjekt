\contentsline {section}{Preface}{i}{Doc-Start}
\contentsline {section}{Acknowledgment}{ii}{section*.1}
\contentsline {section}{Summary and Conclusions}{iii}{section*.2}
\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Background}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Objectives}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Limitations}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Approach}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Structure of the Report}{2}{section.1.5}
\contentsline {chapter}{\numberline {2}Design}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Sensors}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Frame}{3}{section.2.2}
\contentsline {section}{\numberline {2.3}Motors and motor controllers}{3}{section.2.3}
\contentsline {section}{\numberline {2.4}Flight controller}{3}{section.2.4}
\contentsline {section}{\numberline {2.5}Wireless interface}{3}{section.2.5}
\contentsline {section}{\numberline {2.6}Battery}{3}{section.2.6}
\contentsline {chapter}{\numberline {3}Modeling}{4}{chapter.3}
\contentsline {chapter}{\numberline {4}Parameters}{5}{chapter.4}
\contentsline {section}{\numberline {4.1}Inertia matrix}{5}{section.4.1}
\contentsline {section}{\numberline {4.2}Thrust}{5}{section.4.2}
\contentsline {section}{\numberline {4.3}Torque}{5}{section.4.3}
\contentsline {section}{\numberline {4.4}Sensor noise variance}{5}{section.4.4}
\contentsline {chapter}{\numberline {5}Sensor Data Processing}{6}{chapter.5}
\contentsline {chapter}{\numberline {6}Controller}{7}{chapter.6}
\contentsline {section}{\numberline {6.1}Roll, Pich and Yaw controller}{7}{section.6.1}
\contentsline {section}{\numberline {6.2}Position controller}{7}{section.6.2}
\contentsline {section}{\numberline {6.3}Controller modes}{7}{section.6.3}
\contentsline {chapter}{\numberline {7}Testing}{8}{chapter.7}
\contentsline {chapter}{\numberline {8}Summary}{9}{chapter.8}
\contentsline {section}{\numberline {8.1}Summary and Conclusions}{9}{section.8.1}
\contentsline {section}{\numberline {8.2}Discussion}{9}{section.8.2}
\contentsline {section}{\numberline {8.3}Recommendations for Further Work}{9}{section.8.3}
\contentsline {chapter}{\numberline {A}Acronyms}{10}{appendix.A}
\contentsline {chapter}{\numberline {B}Additional Information}{11}{appendix.B}
\contentsline {section}{\numberline {B.1}Introduction}{11}{section.B.1}
\contentsline {subsection}{\numberline {B.1.1}More Details}{11}{subsection.B.1.1}
\contentsline {chapter}{Bibliography}{12}{subsection.B.1.1}
\contentsline {chapter}{Curriculum Vitae}{13}{appendix*.8}
