clear
dt = 0.015;
t_sim = 25;
%dataWithInput = timeseries([phi Dphi theta Dtheta psi Dpsi],time)

%% variance
load('dataNoInput.mat');
systemVar = var(dataNoInput.Data(600:1667,:));
Q_phi = systemVar(1);
Q_theta = systemVar(3);
Q_psi = systemVar(5);

figure(3);
[Pxx,f] = pwelch(dataNoInput.Data(600:1667,3),1667-600,[],[],1/0.015);
plot(f,Pxx);

load('sensorMes.mat');
sensorVar = var(sensorMes.Data);
R_phi = sensorVar(1);
R_theta = sensorVar(3);
R_psi = sensorVar(5);
%% Test data
load('dataWithInput.mat');

%% simulate
input = dataNoInput;
model = 'KalmanFilter';
simout = sim(model, 'Solver', 'ode4', 'StopTime', 't_sim', 'FixedStep', 'dt',...
    'SaveOutput', 'on', 'OutputSaveName', 'y', 'SaveFormat', 'StructureWithTime');
output = simout.get('y');

time = output.time;
data = output.signals.values;

%% Plot
figure(1); clf(1);
subplot(311);hold('on')
plot(input.Time,input.Data(:,1));
plot(time, data(:,1));
title('Angular position')
ylabel('Position [Rad]');
legend('phi, unfiltered','phi, filtered');

subplot(312); hold('on');
plot(input.Time,input.Data(:,3));
plot(time, data(:,3));
ylabel('Position [Rad]');
legend('theta, unfiltered','theta, filtered');

subplot(313); hold('on');
plot(input.Time,input.Data(:,5));
plot(time, data(:,5));
ylabel('Position [Rad]');
xlabel('time [s]');
legend('psi, unfiltered','psi, filtered');

figure(2); clf(2);
subplot(311);hold('on')
plot(input.Time,input.Data(:,2));
plot(time, data(:,2));
title('Angular velocity')
ylabel('Velocity [Rad/s]');
legend('phi, unfiltered','phi, filtered');

subplot(312); hold('on');
plot(input.Time,input.Data(:,4));
plot(time, data(:,4));
ylabel('Velocity [Rad/s]');
legend('theta, unfiltered','theta, filtered');

subplot(313); hold('on');
plot(input.Time,input.Data(:,6));
plot(time, data(:,6));
ylabel('Velocity [Rad/s]');
xlabel('time [s]');
legend('psi, unfiltered','psi, filtered');
